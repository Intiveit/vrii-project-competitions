﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Team
    {
        public int TeamId { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        public List<PersonInTeam> PeopleInTeam { get; set; } = new List<PersonInTeam>();
    }
}
