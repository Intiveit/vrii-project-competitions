﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Person
    {
        public int PersonId { get; set; }

        [MaxLength(100)]
        [Required]
        public string FirstName { get; set; }

        [MaxLength(100)]
        [Required]
        public string LastName { get; set; }

        [MaxLength(25)]
        public string PersonalIdCode { get; set; }

        public string ApplicationUserId { get; set; }

        public List<Registration> Registrations { get; set; } = new List<Registration>();
        public List<Participation> Participations { get; set; } = new List<Participation>();
        public List<PersonInTeam> PersonInTeams { get; set; } = new List<PersonInTeam>();

    }
}
