﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
   public enum County
    {
        None = 0,
        Harjumaa = 1,
        Hiiumaa = 2,
        IdaVirumaa = 3,
        Jõgevamaa = 4,
        Järvamaa = 5,
        Läänemaa = 6,
        LääneVirumaa = 7,
        Pärnumaa = 8,
        Põlvamaa = 9,
        Raplamaa = 10,
        Saaremaa = 11,
        Tartumaa = 12,
        Valgamaa = 13,
        Viljandimaa = 14,
        Võrumaa = 15
    }
}
