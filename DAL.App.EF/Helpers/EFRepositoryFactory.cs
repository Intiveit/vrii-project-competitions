﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.App.EF.Repositories;
using DAL.App.Interfaces.Helpers;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using DAL.Interfaces;

namespace DAL.App.EF.Helpers
{
    public class EFRepositoryFactory : IRepositoryFactory
    {
        private readonly Dictionary<Type, Func<IDataContext, object>> _customRepositoryFactories
            = GetCustomRepoFactories();

        private static Dictionary<Type, Func<IDataContext, object>> GetCustomRepoFactories()
        {
            return new Dictionary<Type, Func<IDataContext, object>>()
            {
                {typeof(ICompetitionTypeRepository), (dataContext) => new EFCompetitionTypeRepository(dataContext as ApplicationDbContext) },
                {typeof(IPersonRepository), (dataContext) => new EFPersonRepository(dataContext as ApplicationDbContext) },
                {typeof(ITeamRepository), (dataContext) => new EFTeamRepository(dataContext as ApplicationDbContext) },
                {typeof(ICompetitionPlaceRepository), (dataContext) => new EFCompetitionPlaceRepository(dataContext as ApplicationDbContext) },
                {typeof(ICompetitionRepository), (dataContext) => new EFCompetitionRepository(dataContext as ApplicationDbContext) },
                {typeof(IParticipationRepository), (dataContext) => new EFParticipationRepository(dataContext as ApplicationDbContext) },
                {typeof(IRegistrationRepository), (dataContext) => new EFRegistrationRepository(dataContext as ApplicationDbContext) },
                {typeof(IPersonInTeamRepository), (dataContext) => new EFPersonInTeamRepository(dataContext as ApplicationDbContext) },



            };
        }

        public Func<IDataContext, object> GetCustomRepositoryFactory<TRepoInterface>() where TRepoInterface : class
        {
            _customRepositoryFactories.TryGetValue(
                typeof(TRepoInterface),
                out Func<IDataContext, object> factory
            );
            return factory;
        }

        public Func<IDataContext, object> GetStandardRepositoryFactory<TEntity>() where TEntity : class
        {

            return (dataContext) => new EFRepository<TEntity>(dataContext as ApplicationDbContext);
        }
    }
}
