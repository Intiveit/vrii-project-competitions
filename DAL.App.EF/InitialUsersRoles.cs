﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace DAL.App.EF
{
    public class InitialUsersRoles
    {
      public static async Task CreateRoles(IServiceProvider serviceProvider)
      {
        ApplicationDbContext context = new ApplicationDbContext();
        var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
        var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();

        var exists = await RoleManager.RoleExistsAsync("admin");
        if (!exists)
        {
          var roleresult = await RoleManager.CreateAsync(new IdentityRole("admin"));
        }

        exists = await RoleManager.RoleExistsAsync("organizer");
        if (!exists)
        {
          var roleresult = await RoleManager.CreateAsync(new IdentityRole("organizer"));
        }

        exists = await RoleManager.RoleExistsAsync("participant");
        if (!exists)
        {
          var roleresult = await RoleManager.CreateAsync(new IdentityRole("participant"));
        }

        var admin = await userManager.FindByEmailAsync("admin@admin.com");

        if (admin == null)
        {
          var user = new ApplicationUser { UserName = "admin@admin.com", Email = "admin@admin.com" };
          var result = await userManager.CreateAsync(user, "Parool1");
          await userManager.AddToRoleAsync(user, "admin");
        }



      }
  }
}
