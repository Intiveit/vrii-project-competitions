﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces.Repositories;
using Domain;

namespace DAL.App.Interfaces.Repositories
{
    public interface ICompetitionRepository : IRepository<Competition>
    {
        bool Exists(int id);
    }
}