﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApp.Models;
using Domain;

namespace WebApp.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Competition> Competitions { get; set; }
        public DbSet<CompetitionPlace> CompetitionPlaces { get; set; }
        public DbSet<CompetitionType> CompetitionTypes { get; set; }
        public DbSet<County> Counties { get; set; }
        public DbSet<Participation> Participations { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<PersonInTeam> PersonInTeams { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<Team> Teams { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
