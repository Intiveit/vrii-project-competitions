﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BL.DTO;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using WebApp.Models.AccountViewModels;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IConfiguration _configuration;

        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IConfiguration configuration, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _roleManager = roleManager;
        }

        /// <summary>
        /// Gets all Users whose role is participant
        /// </summary>
        /// <response code="200">List of users whose role participant</response>
        /// <returns>List of users whose role participant</returns>
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [Route("participants")]
        public async Task<IActionResult> GetParticipants()
        {
            var users = await _userManager.GetUsersInRoleAsync("participant");
            return Ok(users);
        }

        /// <summary>
        /// Gets all Users whose role is organizer
        /// </summary>
        /// <response code="200">List of users whose role organizer</response>
        /// <returns>List of users whose role organizer</returns>
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [Route("organizers")]
        public async Task<IActionResult> GetOrganizers()
        {
            var users = await _userManager.GetUsersInRoleAsync("organizer");
            return Ok(users);

        }

        /// <summary>
        /// Deletes user!
        /// </summary>
        /// <param name="userId">Users ID who will be soon deleted</param>
        /// <response code="200">Deleted user</response>
        /// <returns>Deleted user</returns>
        [HttpDelete("{userId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public async Task<IActionResult> DeleteUser(string userId)
        {
            var user = await _userManager.FindByEmailAsync(userId);
            var result = _userManager.DeleteAsync(user);
            return Ok(result);
        }

        // POST: api/Account/register
        /// <summary>
        /// Add new user
        /// </summary>
        /// <param name="applicationUser">ApplicationUser</param>
        /// <returns>ApplicationUser</returns>
        /// <response code="200">Returns added user</response>
        /// <response code="400">Invalid input</response>
        [HttpPost]
        [Route("register")]

        public async Task<IActionResult> Post([FromBody]ApplicationUserDTO applicationUser)
        {
            if (!ModelState.IsValid) return BadRequest();

            var exists = await _roleManager.RoleExistsAsync(applicationUser.Role);

            if (!exists)
            {
                return BadRequest("Role doesn't exist");
            }

            var user = new ApplicationUser { UserName = applicationUser.Email, Email = applicationUser.Email };
            var result = await _userManager.CreateAsync(user, applicationUser.Password);

            await _userManager.AddToRoleAsync(user, applicationUser.Role);

            if (result.Succeeded)
            {
                return Ok(user);
            }

            return BadRequest(result.Errors);
        }

        /// <summary>
        /// Generates token
        /// </summary>
        /// <param name="loginViewModel">LoginViewModel</param>
        /// <returns>Token</returns>
        /// <response code="200">Returns token</response>
        /// <response code="400">Invalid input</response>
        [HttpPost]
        [AllowAnonymous]
        [Route("getToken")]
        public async Task<IActionResult> GetToken([FromBody] LoginViewModel loginViewModel)
        {
            var user = await _userManager.FindByEmailAsync(loginViewModel.Email);
            var roles = await _userManager.GetRolesAsync(user);
            if (user != null)
            {
                var result = await _signInManager.CheckPasswordSignInAsync(user, loginViewModel.Password, false);
                if (result == Microsoft.AspNetCore.Identity.SignInResult.Success)
                {
                    var options = new IdentityOptions();
                    var claims = new List<Claim>()
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(options.ClaimsIdentity.UserIdClaimType, user.Id.ToString()),
                        new Claim(options.ClaimsIdentity.UserNameClaimType, user.UserName),
                        new Claim(options.ClaimsIdentity.RoleClaimType, roles[0])
                    };

                    var userClaims = await _userManager.GetClaimsAsync(user);
                    claims.AddRange(userClaims);

                    var key = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(_configuration["Token:Key"])
                        );
                    var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                    var token = new JwtSecurityToken(
                        _configuration["Token:Issuer"],
                        _configuration["Token:Issuer"],
                        claims,
                        expires: DateTime.Now.AddMinutes(30),
                        signingCredentials: creds
                        );

                    var res = new
                    {
                        token = new JwtSecurityTokenHandler().WriteToken(token)
                    };

                    return Ok(res);
                }
            }

            return BadRequest("Could not create token");
        }
    }
}