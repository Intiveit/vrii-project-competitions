﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/CompetitionPlaces")]
    public class CompetitionPlacesController : Controller
    {
        private readonly ICompetitionPlaceService _competitionPlaceService;

        public CompetitionPlacesController(ICompetitionPlaceService competitionPlaceService)
        {
            _competitionPlaceService = competitionPlaceService;
        }


        // GET: api/CompetitionPlaces
        /// <summary>
        /// Get list of competition places
        /// </summary>
        /// <returns>List of CompetitionPlaces</returns>
        /// <response code="200">Returns list of CompetitionPlaces</response>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_competitionPlaceService.GetAllCompetitionPlaces());
        }

        // GET: api/CompetitionPlaces/5
        /// <summary>
        /// Get competition place by id 
        /// </summary>
        /// <param name="id">CompetitionPlace's id</param>
        /// <returns>CompetitionPlace</returns>
        /// <response code="200">Returns CompetitionPlace</response>
        /// <response code="404">Id doesn't exist</response>
        [HttpGet("{id}", Name = "GetCompetitionPlace")]
        public IActionResult Get(int id)
        {
            var competitionPlace = _competitionPlaceService.GetCompetitionPlaceById(id);
            if (competitionPlace == null) return NotFound();

            return Ok(competitionPlace);
        }

        // POST: api/CompetitionPlaces
        /// <summary>
        /// Add new competition place
        /// </summary>
        /// <param name="competitionPlace">CompetitionPlace</param>
        /// <returns>Added CompetitionPlace</returns>
        /// <response code="201">Returns added CompetitionPlace</response>
        /// <response code="400">Invalid input</response>
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles="admin, organizer")]

        public IActionResult Post([FromBody]CompetitionPlaceDTO competitionPlace)
        {
            if (!ModelState.IsValid) return BadRequest();

            var newCompetitionPlace = _competitionPlaceService.AddNewCompetitionPlace(competitionPlace);
            return CreatedAtAction("Get", new { id = newCompetitionPlace.CompetitionPlaceId }, newCompetitionPlace);
        }

        // PUT: api/CompetitionPlaces/5
        /// <summary>
        /// Update competition place info
        /// </summary>
        /// <param name="id">Id of competitionPlace to be updated</param>
        /// <param name="competitionPlace">New data of the competitionPlace</param>
        /// <returns>Updated competitionPlace</returns>
        /// <response code="200">Returns updated competitionPlace</response>
        /// <response code="400">CompetitionPlace's data in incorrect format or id doesn't exist</response>
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public IActionResult Put(int id, [FromBody]CompetitionPlaceDTO competitionPlace)
        {
            if (!ModelState.IsValid) return BadRequest();

            var updatedCompetitionPlace = _competitionPlaceService.UpdateCompetitionPlace(id, competitionPlace);

            if (updatedCompetitionPlace == null) return BadRequest();

            return Ok(updatedCompetitionPlace);
        }

        // DELETE: api/CompetitionPlaces/5
        /// <summary>
        /// Deletes competition place by id
        /// </summary>
        /// <param name="id">Id of competitionPlace to be deleted</param>
        /// <returns>Deleted competitionPlace</returns>
        /// <response code="200">Returns deleted competitionPlace</response>
        /// <response code="400">Id doesn't exist</response>
        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public IActionResult Delete(int id)
        {
            var deletedCompetitionPlace = _competitionPlaceService.DeleteCompetitionPlace(id);
            if (deletedCompetitionPlace == null) return BadRequest();

            return Ok(deletedCompetitionPlace);

        }
    }
}