﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.Services;
using DAL.App.Interfaces;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/CompetitionTypes")]
    public class CompetitionTypesController : Controller
    {
        private readonly ICompetitionTypeService _competitionTypeService;

        public CompetitionTypesController(ICompetitionTypeService competitionTypeService)
        {
            _competitionTypeService = competitionTypeService;
        }


        // GET: api/CompetitionTypes
        /// <summary>
        /// Get list of competition types
        /// </summary>
        /// <returns>List of CompetitionTypes</returns>
        /// <response code="200">Returns list of CompetitionTypes</response>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_competitionTypeService.GetAllCompetitionTypes());
        }

        // GET: api/CompetitionTypes/5
        /// <summary>
        /// Get competition type by id 
        /// </summary>
        /// <param name="id">CompetitionType's id</param>
        /// <returns>CompetitionType</returns>
        /// <response code="200">Returns CompetitionType</response>
        /// <response code="404">Id doesn't exist</response>
        [HttpGet("{id}", Name = "GetCompetitionType")]
        public IActionResult Get(int id)
        {
            var competitionType = _competitionTypeService.GetCompetitionTypeById(id);
            if (competitionType == null) return NotFound();

            return Ok(competitionType);
        }

        // POST: api/CompetitionTypes
        /// <summary>
        /// Add new competition type
        /// </summary>
        /// <param name="competitionType">CompetitionType</param>
        /// <returns>Added CompetitionType</returns>
        /// <response code="201">Returns added CompetitionType</response>
        /// <response code="400">Invalid input</response>
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin, organizer")]
        public IActionResult Post([FromBody]CompetitionTypeDTO competitionType)
        {
            if (!ModelState.IsValid) return BadRequest();

            var newCompetitionType = _competitionTypeService.AddNewCompetitionType(competitionType);
            return CreatedAtAction("Get", new {id = newCompetitionType.CompetitionTypeId}, newCompetitionType);
        }

        // PUT: api/CompetitionTypes/5
        /// <summary>
        /// Update competition type info
        /// </summary>
        /// <param name="id">Id of CompetitionType to be updated</param>
        /// <param name="competitionType">New data of the CompetitionType</param>
        /// <returns>Updated CompetitionType</returns>
        /// <response code="200">Returns updated CompetitionType</response>
        /// <response code="400">CompetitionType's data in incorrect format or id doesn't exist</response>
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public IActionResult Put(int id, [FromBody]CompetitionTypeDTO competitionType)
        {
            if (!ModelState.IsValid) return BadRequest();

            var updatedCompetitionType = _competitionTypeService.UpdateCompetitionType(id, competitionType);

            if (updatedCompetitionType == null) return BadRequest();

            return Ok(updatedCompetitionType);
        }

        // DELETE: api/CompetitionType/5
        /// <summary>
        /// Delete competition type by id
        /// </summary>
        /// <param name="id">Id of CompetitionType to be deleted</param>
        /// <returns>Deleted CompetitionType</returns>
        /// <response code="200">Returns deleted CompetitionType</response>
        /// <response code="400">Id doesn't exist</response>
        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public IActionResult Delete(int id)
        {
            var deletedCompetitionType = _competitionTypeService.DeleteCompetitionType(id);
            if (deletedCompetitionType == null) return BadRequest();

            return Ok(deletedCompetitionType);

        }
    }
}
