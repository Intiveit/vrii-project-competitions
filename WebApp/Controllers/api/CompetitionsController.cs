﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using DAL.App.EF.Repositories;
using Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/Competitions")]
    public class CompetitionsController : Controller
    {
        private readonly ICompetitionService _competitionService;

        public CompetitionsController(ICompetitionService competitionService)
        {
            _competitionService = competitionService;
        }


        // GET: api/Competitions
        /// <summary>
        /// Get list of competitions
        /// </summary>
        /// <returns>List of competitions</returns>
        /// <response code="200">Returns list of competitions</response>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_competitionService.GetAllCompetitions());
        }

        // GET: api/Competitions/5
        /// <summary>
        /// Get competition by id 
        /// </summary>
        /// <param name="id">competition's id</param>
        /// <returns>Competition</returns>
        /// <response code="200">Returns competition</response>
        /// <response code="404">Id doesn't exist</response>
        [HttpGet("{id}", Name = "GetCompetition")]
        public IActionResult Get(int id)
        {
            var competition = _competitionService.GetCompetitionById(id);
            if (competition == null) return NotFound();

            return Ok(competition);
        }

        // POST: api/Competitions
        /// <summary>
        /// Add new competition
        /// </summary>
        /// <param name="competition">Competition</param>
        /// <returns>Added competition</returns>
        /// <response code="201">Returns added competition</response>
        /// <response code="400">Invalid input</response>
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin, organizer")]
        public IActionResult Post([FromBody]CompetitionDTO competition)
        {
            if (!ModelState.IsValid) return BadRequest();

            var newCompetition = _competitionService.AddNewCompetition(competition, User.Identity.GetUserId());

            if (newCompetition == null) return BadRequest();
            return CreatedAtAction("Get", new { id = newCompetition.CompetitionId }, newCompetition);
        }

        // PUT: api/Competitions/5
        /// <summary>
        /// Update competition info
        /// </summary>
        /// <param name="id">Id of competition to be updated</param>
        /// <param name="competition">New data of the competition</param>
        /// <returns>Updated competition</returns>
        /// <response code="200">Returns updated competition</response>
        /// <response code="400">Competition's data in incorrect format or id doesn't exist</response>
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin, organizer")]
        public IActionResult Put(int id, [FromBody]CompetitionDTO competition)
        {
            if (!ModelState.IsValid) return BadRequest();

            var updatedCompetition = _competitionService.UpdateCompetition(id, competition, User.Identity.GetUserId(), User.IsInRole("admin"));

            if (updatedCompetition == null) return BadRequest();

            return Ok(updatedCompetition);
        }

        // DELETE: api/Competitions/5
        /// <summary>
        /// Delete competition by id
        /// </summary>
        /// <param name="id">Id of competition to be deleted</param>
        /// <returns>Deleted competition</returns>
        /// <response code="200">Returns deleted competition</response>
        /// <response code="400">Id doesn't exist</response>
        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin, organizer")]
        public IActionResult Delete(int id)
        {
            var deletedCompetition = _competitionService.DeleteCompetition(id, User.Identity.GetUserId(), User.IsInRole("admin"));
            if (deletedCompetition == null) return BadRequest();

            return Ok(deletedCompetition);

        }
    }
}