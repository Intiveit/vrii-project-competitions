﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;

namespace BL.Services
{
    public class RegistrationService: IRegistrationService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IRegistrationFactory _registrationFactory;

        public RegistrationService(IAppUnitOfWork uow, IRegistrationFactory registrationFactory)
        {
            _uow = uow;
            _registrationFactory = registrationFactory;
        }

        /// <summary>
        /// Gets all registrations
        /// </summary>
        /// <returns> IEnumerable of Registration</returns>
        public IEnumerable<RegistrationDTO> GetAllRegistrations()
        {
            return _uow.Registrations.All().Select(r => _registrationFactory.Create(r));
        }

        /// <summary>
        /// Gets one specific registration by its id
        /// </summary>
        /// <param name="id">Registration id</param>
        /// <returns>RegistrationDTO type registration</returns>
        public RegistrationDTO GetRegistrationById(int id)
        {
            var r = _uow.Registrations.Find(id);
            if (r == null) return null;

            return _registrationFactory.Create(r);
        }

        /// <summary>
        /// Gets List of registrations for competition
        /// </summary>
        /// <param name="competitionId">Competition id</param>
        /// <returns>List of RegistrationsDTO's</returns>
        public IEnumerable<RegistrationDTO> GetAllRegistrationsForCompetition(int competitionId)
        {
            return _uow.Registrations.All().Where(c => c.CompetitionId == competitionId).Select(ct => _registrationFactory.Create(ct));
        }

        /// <summary>
        /// Gets List of registrations for person
        /// </summary>
        /// <param name="personId">Person's id</param>
        /// <returns>List of RegistrationsDTO's</returns>
        public IEnumerable<RegistrationDTO> GetAllRegistrationsForPerson(int personId)
        {
            return _uow.Registrations.All().Where(p => p.PersonId == personId).Select(ct => _registrationFactory.Create(ct));
        }

        /// <summary>
        /// Gets List of registrations for team
        /// </summary>
        /// <param name="teamId">Team's id</param>
        /// <returns>List of RegistrationsDTO's</returns>
        public IEnumerable<RegistrationDTO> GetAllRegistrationsForTeam(int teamId)
        {
            return _uow.Registrations.All().Where(p => p.TeamId == teamId).Select(ct => _registrationFactory.Create(ct));
        }

        /// <summary>
        /// Adds new registration
        /// </summary>
        /// <param name="dto">RegistrationDTO type object</param>
        /// <param name="userId">Registration creater user id</param>
        /// <returns>Added registration</returns>
        public RegistrationDTO AddNewRegistration(RegistrationDTO dto, string userId)
        {
            var newRegistration = _registrationFactory.Create(dto);
            if (newRegistration == null) return null;

            newRegistration.ApplicationUserId = userId;

            if (!_uow.Competitions.Exists(dto.CompetitionId) ||
                !_uow.People.Exists(dto.PersonId.GetValueOrDefault()) && 
                !_uow.Teams.Exists(dto.TeamId.GetValueOrDefault()))
            {
                return null;
            }

            _uow.Registrations.Add(newRegistration);
            _uow.SaveChanges();

            return _registrationFactory.Create(newRegistration);
        }


        /// <summary>
        /// Deletes registration by its id
        /// </summary>
        /// <param name="id">Id of Registration to be deleted</param>
        /// <param name="userId">Registration deleter user id</param>
        /// <param name="isAdmin">Registration deleter admin id</param>
        /// <returns>Returns deleted registration</returns>
        public RegistrationDTO DeleteRegistration(int id, string userId, bool isAdmin)
        {
            var r = _uow.Registrations.Find(id);
            if (r == null) return null;

            if (r.ApplicationUserId != userId &&  !isAdmin)
            {
                return null;
            }

            _uow.Registrations.Remove(r);
            _uow.SaveChanges();

            return _registrationFactory.Create(r);
        }
    }
}
