﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using DAL.App.Interfaces;

namespace BL.Services
{
    public class PersonService : IPersonService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IPersonFactory _personFactory;

        public PersonService(IAppUnitOfWork uow, IPersonFactory personFactory)
        {
            _uow = uow;
            _personFactory = personFactory;
        }

        /// <summary>
        /// Gets all people
        /// </summary>
        /// <returns> IEnumerable of Person</returns>
        public IEnumerable<PersonDTO> GetAllPersons()
        {
            return _uow.People.All().Select(p => _personFactory.Create(p));
        }

        /// <summary>
        /// Gets one specific person by its id
        /// </summary>
        /// <param name="id">Person id</param>
        /// <returns>PersonDTO type person</returns>
        public PersonDTO GetPersonById(int id)
        {
            var p = _uow.People.Find(id);
            if (p == null) return null;

            return _personFactory.Create(p);
        }

        /// <summary>
        /// Gets one specific person by its email
        /// </summary>
        /// <param name="email">Person's email</param>
        /// <returns>PersonDTO type person</returns>
        public PersonDTO GetPersonByEmail(string email)
        {
            var p = _uow.People.All().FirstOrDefault(person => person.ApplicationUserId == email);
            if (p == null) return null;

            return _personFactory.Create(p);
        }

        /// <summary>
        /// Gets one specific person by its personal IDcode
        /// </summary>
        /// <param name="idCode">Person's IDcode</param>
        /// <returns>PersonDTO type person</returns>
        public PersonDTO GetPersonByIdCode(string idCode)
        {
            var p = _uow.People.All().FirstOrDefault(person => person.PersonalIdCode == idCode);
            if (p == null) return null;

            return _personFactory.Create(p);
        }

        /// <summary>
        /// Gets list of people in team
        /// </summary>
        /// <param name="teamId">Team's id</param>
        /// <returns>List of PersonDTO's</returns>
        public IEnumerable<PersonDTO> GetPeopleByTeam(int teamId)
        {
            if (!_uow.Teams.Exists(teamId))
            {
                return null;
            }
            var pit = _uow.PersonInTeams.All().Where(x => x.TeamId == teamId).Select(p=> p.PersonId).ToList();
            return _uow.People.All().Where(x => pit.Contains(x.PersonId)).Select(p => _personFactory.Create(p));
        }

        /// <summary>
        /// Adds new person
        /// </summary>
        /// <param name="dto">PersonDTO type object</param>
        /// <returns>Added person</returns>
        public PersonDTO AddNewPerson(PersonDTO dto)
        {
            var newPerson = _personFactory.Create(dto);
            if (newPerson == null) return null;

            _uow.People.Add(newPerson);
            _uow.SaveChanges();

            return _personFactory.Create(newPerson);
        }

        /// <summary>
        /// Updates one specific person data
        /// </summary>
        /// <param name="id">Id of Person to be updated</param>
        /// <param name="dto">New dataof Person</param>
        /// <param name="userId">Person updater user id</param>
        /// <param name=isAdmin">Person updater admin id</param>
        /// <returns>Returns updated Person</returns>
        public PersonDTO UpdatePerson(int id, PersonDTO dto, string userId, bool isAdmin)
        {
            var p = _uow.People.Find(id);

            if (p == null) return null;

            if (p.ApplicationUserId != userId && !isAdmin)
            {
                return null;
            }

            p.FirstName = dto.FirstName;
            p.LastName = dto.LastName;

            _uow.SaveChanges();

            return _personFactory.Create(p);
        }

        /// <summary>
        /// Deletes person by its id
        /// </summary>
        /// <param name="id">Id of Person to be deleted</param>
        /// <param name="userId">Person deleter user id</param>
        /// <param name="isAdmin">Person deleter admin id</param>
        /// <returns>Returns deleted persone</returns>
        public PersonDTO DeletePerson(int id, string userId, bool isAdmin)
        {
            var p = _uow.People.Find(id);
            if (p == null) return null;

            if (p.ApplicationUserId != userId && !isAdmin )
            {
                return null;
            }

            _uow.People.Remove(p);
            _uow.SaveChanges();

            return _personFactory.Create(p);
        }
    }
}
