﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services
{
    public interface ICompetitionTypeService
    {
        IEnumerable<CompetitionTypeDTO> GetAllCompetitionTypes();
        CompetitionTypeDTO GetCompetitionTypeById(int id);
        CompetitionTypeDTO AddNewCompetitionType(CompetitionTypeDTO dto);
        CompetitionTypeDTO UpdateCompetitionType(int id, CompetitionTypeDTO dto);
        CompetitionTypeDTO DeleteCompetitionType(int id);
    }
}
