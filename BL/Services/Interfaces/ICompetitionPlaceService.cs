﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface ICompetitionPlaceService
    {
        IEnumerable<CompetitionPlaceDTO> GetAllCompetitionPlaces();
        CompetitionPlaceDTO GetCompetitionPlaceById(int id);
        CompetitionPlaceDTO AddNewCompetitionPlace(CompetitionPlaceDTO dto);
        CompetitionPlaceDTO UpdateCompetitionPlace(int id, CompetitionPlaceDTO dto);
        CompetitionPlaceDTO DeleteCompetitionPlace(int id);
    }
}
