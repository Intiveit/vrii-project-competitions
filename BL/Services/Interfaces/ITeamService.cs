﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface ITeamService
    {

        TeamDTO GetTeamById(int id);
        TeamDTO AddNewTeam(TeamDTO dto);
        TeamDTO UpdateTeam(int id, TeamDTO dto, string userId);
        TeamDTO DeleteTeam(int id);
    }
}
