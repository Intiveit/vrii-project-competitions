﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface ICompetitionService
    {
        IEnumerable<CompetitionDTO> GetAllCompetitions();
        CompetitionDTO GetCompetitionById(int id);
        CompetitionDTO AddNewCompetition(CompetitionDTO dto, string userId);
        CompetitionDTO UpdateCompetition(int id, CompetitionDTO dto, string userId, bool isAdmin);
        CompetitionDTO DeleteCompetition(int id, string userId, bool isAdmin);
    }
}
