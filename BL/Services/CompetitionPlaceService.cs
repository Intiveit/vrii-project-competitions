﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;

namespace BL.Services
{
    public class CompetitionPlaceService : ICompetitionPlaceService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly ICompetitionPlaceFactory _competitionPlaceFactory;

        public CompetitionPlaceService(IAppUnitOfWork uow, ICompetitionPlaceFactory competitionPlaceFactory)
        {
            _uow = uow;
            _competitionPlaceFactory = competitionPlaceFactory;
        }

        /// <summary>
        /// Gets all competition places
        /// </summary>
        /// <returns> IEnumerable of CompetitionPlaces</returns>
        public IEnumerable<CompetitionPlaceDTO> GetAllCompetitionPlaces()
        {
            return _uow.CompetitionPlaces.All()
                .Select(cp => _competitionPlaceFactory.Create(cp));
        }

        /// <summary>
        /// Gets one specific competition place by its id
        /// </summary>
        /// <param name="id">CompetitionPlace id</param>
        /// <returns>CompetitionPlaceDTO type competition place</returns>
        public CompetitionPlaceDTO GetCompetitionPlaceById(int id)
        {
            var competitionPlace = _uow.CompetitionPlaces.Find(id);
            if (competitionPlace == null) return null;

            return _competitionPlaceFactory.Create(competitionPlace);
        }

        /// <summary>
        /// Adds new competition place
        /// </summary>
        /// <param name="dto">CompetitionPlaceDTO type object</param>
        /// <returns>Added competition place</returns>
        public CompetitionPlaceDTO AddNewCompetitionPlace(CompetitionPlaceDTO dto)
        {
            var newCompetitionPlace = _competitionPlaceFactory.Create(dto);
            _uow.CompetitionPlaces.Add(newCompetitionPlace);
            _uow.SaveChanges();
            return _competitionPlaceFactory.Create(newCompetitionPlace);
        }

        /// <summary>
        /// Updates one specific competition place data
        /// </summary>
        /// <param name="id">Id of CompetitionPlace to be updated</param>
        /// <param name="dto">New dataof CompetitionPlace</param>
        /// <returns>Returns updated CompetitionPlace</returns>
        public CompetitionPlaceDTO UpdateCompetitionPlace(int id, CompetitionPlaceDTO dto)
        {
            var competitionPlace = _uow.CompetitionPlaces.Find(id);

            if (competitionPlace == null) return null;

            competitionPlace.Name = dto.Name;
            competitionPlace.Address = dto.Address;
            competitionPlace.County = dto.County;
            competitionPlace.Comment = dto.Comment;

            _uow.SaveChanges();

            return _competitionPlaceFactory.Create(competitionPlace);
        }

        /// <summary>
        /// Deletes competition place by its id
        /// </summary>
        /// <param name="id">Id of CompetitionPlace to be deleted</param>
        /// <returns>Returns deleted competition place</returns>
        public CompetitionPlaceDTO DeleteCompetitionPlace(int id)
        {
            var competitionPlace = _uow.CompetitionPlaces.Find(id);
            if (competitionPlace == null) return null;

            _uow.CompetitionTypes.Remove(competitionPlace);
            _uow.SaveChanges();

            return _competitionPlaceFactory.Create(competitionPlace);
        }
    }
}
