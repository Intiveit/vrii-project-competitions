﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BL.DTO
{
    public class ParticipationDTO
    {
        public int ParticipationId { get; set; }
        public int? PersonId { get; set; }
        public int? TeamId { get; set; }

        [Required]
        public int CompetitionId { get; set; }

        public double Points { get; set; }
        public TimeSpan Time { get; set; }
        public bool IsDisqualified { get; set; }
        public string PersonFirstName { get; set; }
        public string PersonLastName { get; set; }
        public string CompetitionName { get; set; }
        public string TeamName { get; set; }
    }
}
