﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BL.DTO
{
    public class PersonDTO
    {
        public int PersonId { get; set; }

        [MaxLength(100)]
        [Required]
        public string FirstName { get; set; }

        [MaxLength(100)]
        [Required]
        public string LastName { get; set; }

        [MaxLength(25)]
        public string PersonalIdCode { get; set; }

        public string ApplicationUserId { get; set; }
    }
}
