﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class CompetitionPlaceDTO
    {
        public int CompetitionPlaceId { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [Required]
        public County County { get; set; }

        [MaxLength(200)]
        [Required]
        public string Address { get; set; }

        [MaxLength(1000)]
        public string Comment { get; set; }

    }
}
