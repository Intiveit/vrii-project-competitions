﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BL.DTO
{
    public class CompetitionDTO
    {
        public int CompetitionId { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [Required]
        public int CompetitionTypeId { get; set; }       

        [Required]
        public int CompetitionPlaceId { get; set; }

        [Required]
        public DateTime Time { get; set; }

        [MaxLength(1000)]
        public string Comment { get; set; }

        public string ApplicationUserId { get; set; }
    }
}
