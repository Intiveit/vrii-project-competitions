﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{

    public class CompetitionTypeFactory : ICompetitionTypeFactory
    {
        /// <summary>
        /// Creates CompetitionTypeDTO type object
        /// </summary>
        /// <param name="competitionType"> CompetitionType type object competitionType</param>
        /// <returns>new CompetitionTypeDTO type object</returns>
        public CompetitionTypeDTO Create(CompetitionType competitionType)
        {
            return new CompetitionTypeDTO()
            {
                CompetitionTypeId = competitionType.CompetitionTypeId,
                Name = competitionType.Name,
                HasPoints = competitionType.HasPoints,
                HasTime = competitionType.HasTime,
                IsIndividual = competitionType.IsIndividual
            };
        }

        /// <summary>
        /// Creates CompetitionType type object
        /// </summary>
        /// <param name="dto"> CompetitionTypeDTO type object</param>
        /// <returns>new CompetitionType type object</returns>

        public CompetitionType Create(CompetitionTypeDTO dto)
        {
            return new CompetitionType()
            {
                CompetitionTypeId = dto.CompetitionTypeId,
                Name = dto.Name,
                HasPoints = dto.HasPoints,
                HasTime = dto.HasTime,
                IsIndividual = dto.IsIndividual
            };
        }
    }
}
