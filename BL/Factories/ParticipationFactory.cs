﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services;
using BL.Services.Interfaces;
using Domain;

namespace BL.Factories
{

    public class ParticipationFactory : IParticipationFactory
    {
        private IPersonService _personService;
        private ICompetitionService _competitionService;
        private ITeamService _teamService;

        public ParticipationFactory(IPersonService personService, ICompetitionService competitonService, ITeamService teamService)
        {
            _personService = personService;
            _competitionService = competitonService;
            _teamService = teamService;
        }

        /// <summary>
        /// Creates ParticipationDTO type object
        /// </summary>
        /// <param name="participation"> Participation type object participation</param>
        /// <returns>new ParticipationDTO type objcet</returns>
        public ParticipationDTO Create(Participation participation)
        {

            var competition = _competitionService.GetCompetitionById(participation.CompetitionId);

            var dto = new ParticipationDTO() 
            {
                ParticipationId = participation.ParticipationId,
                CompetitionId = participation.CompetitionId,
                Points = participation.Points,
                Time = participation.Time,
                IsDisqualified = participation.IsDisqualified,                
                CompetitionName = competition.Name,
                

            };
            if (participation.PersonId != null)
            {
                int id = participation.PersonId ?? default(int);
                var person = _personService.GetPersonById(id);
                dto.PersonId = participation.PersonId;
                dto.PersonFirstName = person.FirstName;
                dto.PersonLastName = person.LastName;
                dto.TeamId = null;
            }
            else
            {
                int id = participation.TeamId ?? default(int);
                var team = _teamService.GetTeamById(id);
                dto.TeamId = participation.TeamId;
                dto.TeamName = team.Name;
                dto.PersonId = null;
            }

            return dto;
        }

        /// <summary>
        /// Creates Participation type object
        /// </summary>
        /// <param name="dto"> ParticipationDTO type object</param>
        /// <returns>new Participation type object</returns>
        public Participation Create(ParticipationDTO dto)
        {
            var participation = new Participation()
            {
                ParticipationId = dto.ParticipationId,
                PersonId = dto.PersonId,
                CompetitionId = dto.CompetitionId,
                Points = dto.Points,
                Time = dto.Time,
                IsDisqualified = dto.IsDisqualified,
                TeamId = dto.TeamId
            };
            if (participation.PersonId == 0) participation.PersonId = null;
            if (participation.TeamId == 0) participation.TeamId = null;
            return participation;
        }
    }
}
