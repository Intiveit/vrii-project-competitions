﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class TeamFactory: ITeamFactory
    {
        /// <summary>
        /// Creates TeamDTO type object
        /// </summary>
        /// <param name="t"> Team type object t</param>
        /// <returns>new TeamDTO type object</returns>
        public TeamDTO Create(Team t)
        {
            return new TeamDTO()
            {
                TeamId = t.TeamId,
                Name = t.Name
            };
        }

        /// <summary>
        /// Creates Team type object
        /// </summary>
        /// <param name="dto"> TeamDTO type object</param>
        /// <returns>new Team type object</returns>
        public Team Create(TeamDTO dto)
        {
            return new Team()
            {
                TeamId = dto.TeamId,
                Name = dto.Name
            };
        }
    }
}
