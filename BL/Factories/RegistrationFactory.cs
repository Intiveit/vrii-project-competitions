﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services;
using BL.Services.Interfaces;
using Domain;

namespace BL.Factories
{
    public class RegistrationFactory : IRegistrationFactory
    {
        private IPersonService _personService;
        private ICompetitionService _competitionService;
        private ITeamService _teamService;

        public RegistrationFactory(IPersonService personsService, ICompetitionService competitonService, ITeamService teamService)
        {
            _personService = personsService;
            _competitionService = competitonService;
            _teamService = teamService;
        }

        /// <summary>
        /// Creates RegistrationDTO type object
        /// </summary>
        /// <param name="r"> Registration type object r</param>
        /// <returns>new RegistrationDTO type object</returns>
        public RegistrationDTO Create(Registration r)
        {
            var competition = _competitionService.GetCompetitionById(r.CompetitionId);

            var dto = new RegistrationDTO()
            {
                RegistrationId = r.RegistrationId,
                CompetitionId = r.CompetitionId,
                CompetitionName = competition.Name,
                CompetitionTime = competition.Time,
            };
            if (r.PersonId != null)
            {
                int id = r.PersonId ?? default(int);
                var person = _personService.GetPersonById(id);
                dto.PersonId = r.PersonId;
                dto.PersonFirstName = person.FirstName;
                dto.PersonLastName = person.LastName;
                dto.TeamId = null;
            }
            else
            {
                int id = r.TeamId ?? default(int);
                var team = _teamService.GetTeamById(id);
                dto.TeamId = r.TeamId;
                dto.TeamName = team.Name;
                dto.PersonId = null;
            }

            return dto;
        }
    

        /// <summary>
        /// Creates Registration type object
        /// </summary>
        /// <param name="dto"> RegistrationDTO type object</param>
        /// <returns>new Registration type object</returns>
        public Registration Create(RegistrationDTO dto)
        {
            var registration = new Registration()
            {
                RegistrationId = dto.RegistrationId,
                PersonId = dto.PersonId,
                CompetitionId = dto.CompetitionId,
                TeamId = dto.TeamId
            };
            if (registration.PersonId == 0) registration.PersonId = null;
            if (registration.TeamId == 0) registration.TeamId = null;
            return registration;

        }
    }
}
