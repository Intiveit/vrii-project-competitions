﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IParticipationFactory
    {
        ParticipationDTO Create(Participation participation);
        Participation Create(ParticipationDTO dto);
    }
}
