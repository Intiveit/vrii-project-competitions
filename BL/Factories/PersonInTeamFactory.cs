﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services;
using BL.Services.Interfaces;
using Domain;

namespace BL.Factories
{
    public class PersonInTeamFactory: IPersonInTeamFactory
    {
        private ITeamService _teamService;

        public PersonInTeamFactory(ITeamService teamService)
        {
            _teamService = teamService;
        }

        /// <summary>
        /// Creates PersonInTeamDTO type object
        /// </summary>
        /// <param name="pit"> PersonInTeam type object pit</param>
        /// <returns>new PersonInTeamDTO type object</returns>
        public PersonInTeamDTO Create(PersonInTeam pit)
        {
            var team = _teamService.GetTeamById(pit.TeamId);

            return new PersonInTeamDTO()
            {
                PersonInTeamId = pit.PersonInTeamId,
                PersonId = pit.PersonId,
                TeamId = pit.TeamId,
                TeamName = team.Name
            };
        }

        /// <summary>
        /// Creates PersonInTeam type object
        /// </summary>
        /// <param name="dto"> PersonInTeamDTO type object</param>
        /// <returns>new PersonInTeam type object</returns>
        public PersonInTeam Create(PersonInTeamDTO dto)
        {
            return new PersonInTeam()
            {
                PersonInTeamId = dto.PersonInTeamId,
                PersonId = dto.PersonId,
                TeamId = dto.TeamId
            };
        }
    }
}
