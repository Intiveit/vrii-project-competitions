﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for AddNotUser.xaml
    /// </summary>
    public partial class EditTeam : Window
    {
        private Team _team;
        private EditTeamVM _vm;
        public EditTeam(Team team)
        {
            _team = team;
            InitializeComponent();
            this.Loaded += EditTeam_Loaded;
        }
        private void EditTeam_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new EditTeamVM();
            txtName.Text = _team.Name;
        }



        private async void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (txtName.Text == "")
            {
                MessageBox.Show("Nimi peab olema täidetud!", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var t = await _vm.UpdateTeam(_team, txtName.Text);
            TeamInfo newWindow = new TeamInfo(t);
            newWindow.Show();
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            TeamInfo newWindow = new TeamInfo(_team);
            newWindow.Show();
            this.Close();
        }

    }
}
