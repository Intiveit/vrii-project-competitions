﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for AddUser.xaml
    /// </summary>
    public partial class AddUser : Window
    {
        private Team _team;
        private AddUserVM _vm;

        public AddUser(Team team)
        {
            _team = team;
            _vm = new AddUserVM();
            InitializeComponent();
        }

        private async void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (txtEmail.Text == "")
            {
                MessageBox.Show("Kõik väljad peavad olema täidetud!", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var result = await _vm.AddTeamMember(_team, txtEmail.Text);
            if (result == null)
            {
                MessageBox.Show("Liikme lisamine ebaõnnestus!", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Members newWindow = new Members(_team);
            newWindow.Show();
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Members newWindow = new Members(_team);
            newWindow.Show();
            this.Close();
        }
    }
}
