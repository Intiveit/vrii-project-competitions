﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for AddPlace.xaml
    /// </summary>
    public partial class AddCompetitionPlace : Window
    {
        private AddCompetitionPlaceVM _vm;
        public AddCompetitionPlace()
        {
            InitializeComponent();
            cmbCounty.Items.Add("Harjumaa");
            cmbCounty.Items.Add("Hiiumaa");
            cmbCounty.Items.Add("IdaVirumaa");
            cmbCounty.Items.Add("Jõgevamaa");
            cmbCounty.Items.Add("Järvamaa");
            cmbCounty.Items.Add("Läänemaa");
            cmbCounty.Items.Add("LääneVirumaa");
            cmbCounty.Items.Add("Pärnumaa");
            cmbCounty.Items.Add("Põlvamaa");
            cmbCounty.Items.Add("Raplamaa");
            cmbCounty.Items.Add("Saaremaa");
            cmbCounty.Items.Add("Tartumaa");
            cmbCounty.Items.Add("Valgamaa");
            cmbCounty.Items.Add("Viljandimaa");
            cmbCounty.Items.Add("Võrumaa");
            _vm = new AddCompetitionPlaceVM();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            var newAddCompetition = new AddCompetition();
            newAddCompetition.Show();
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (txtPlaceName.Text == "" || txtAddress.Text == "" || cmbCounty.Text == "")
            {
                MessageBox.Show("Koha nimetuse, aadressi ja maakonna väljad peavad olema täidetud!", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            _vm.CreateCompetitionPlace(txtPlaceName.Text, txtAddress.Text,cmbCounty.Text,txtComment.Text);

            var newAddCompetition = new AddCompetition();
            newAddCompetition.Show();
            this.Close();
        }
    }
}
