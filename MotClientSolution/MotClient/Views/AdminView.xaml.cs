﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for AdminView.xaml
    /// </summary>
    public partial class AdminView : Window
    {
        private AdminViewVM _vm;

        public AdminView()
        {
            InitializeComponent();
            this.Loaded += AdminView_Loaded;
        }
        private void AdminView_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new AdminViewVM();
            Task.Run(() => _vm.LoadData()).Wait();
            this.DataContext = _vm;
        }

        private async void btnDeleteParticipant_Click(object sender, RoutedEventArgs e)
        {
            Person person = lbParticipants.SelectedItem as Person;
            await _vm.DeleteParticipant(person);
            AdminView newResults = new AdminView();
            newResults.Show();
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow newWindow = new MainWindow();
            newWindow.Show();
            this.Close();
        }

        private void btnAddParticipant_Click(object sender, RoutedEventArgs e)
        {
            Registreeri newWindow = new Registreeri();
            newWindow.Show();
            this.Close();
        }

        private void btnAddorganizer_Click(object sender, RoutedEventArgs e)
        {
            Registreeri newWindow = new Registreeri();
            newWindow.Show();
            this.Close();
        }

        private async void btnDeleteOrganizer_Click(object sender, RoutedEventArgs e)
        {
            User user = lbOrganizers.SelectedItem as User;
            await _vm.DeleteOrganizer(user);
            AdminView newResults = new AdminView();
            newResults.Show();
            this.Close();
        }
    }
}
