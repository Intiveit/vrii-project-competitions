﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for Registreeri.xaml
    /// </summary>
    public partial class Registreeri : Window
    {
        private RegistrationVM _vm;

        public Registreeri()
        {
            InitializeComponent();
            cmbType.Items.Add("korraldaja");
            cmbType.Items.Add("osaleja");
            this.cmbType.SelectionChanged += new SelectionChangedEventHandler(OnTypeChanged);
            _vm = new RegistrationVM();
        }



        private void OnTypeChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbType.SelectedValue.ToString() == "osaleja")
            {
                gridPersonInfo.Visibility = Visibility.Visible;
            }
            else
            {
                gridPersonInfo.Visibility = Visibility.Hidden;
            }
        }

        private async void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            if (cmbType.SelectedValue == null|| txtEmail.Text == "" || txtPassword.Password == "")
            {
                MessageBox.Show("Kõik väljad peavad olema täidetud!", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (cmbType.SelectedValue.ToString() == "osaleja")
            {
                if (txtFirstname.Text == "" || txtLastname.Text == "" || txtPersonalIdCode.Text == "")
                {
                    MessageBox.Show("Kõik väljad peavad olema täidetud!", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            if (txtPassword.Password.Length < 6)
            {
                MessageBox.Show("Parool peab olema vähemalt 6 tähemärki pikk!", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (cmbType.SelectedValue.ToString() == "osaleja")
            {
                var idCode = await _vm.CheckID(txtPersonalIdCode.Text);
                if (idCode != null)
                {
                    MessageBox.Show(" Sellise isikukoodiga on juba registreerutud!",
                        "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            var response = await _vm.CreateUser(cmbType.SelectedValue.ToString(), txtEmail.Text, txtPassword.Password, txtFirstname.Text,
            txtLastname.Text, txtPersonalIdCode.Text);

            if (response == null)
            {
                MessageBox.Show("Kasutaja loomine ebaõnnestus. \n Valitud email on juba kasutusel või emaili formaat on vale.", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            MainWindow newWindow = new MainWindow();
            newWindow.Show();
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow newWindow = new MainWindow();
            newWindow.Show();
            this.Close();
        }
    }
}
