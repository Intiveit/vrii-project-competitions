﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for RegistrateTeam.xaml
    /// </summary>
    public partial class RegistrateTeam : Window
    {
        private RegistrateTeamVM _vm;
        public Competition Competition;
        public RegistrateTeam(Competition competition)
        {
            Competition = competition;
            InitializeComponent();
            this.Loaded += RegistrateTeam_Loaded;
        }
        private void RegistrateTeam_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new RegistrateTeamVM();
            Task.Run(() => _vm.LoadData()).Wait();
            this.DataContext = _vm;
        }
    

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Competitions newWindow = new Competitions(Competition);
            newWindow.Show();
            this.Close();
        }

        private async void btnRegistrate_Click(object sender, RoutedEventArgs e)
        {
            PersonInTeam pit;
            try
            {
                pit = (PersonInTeam) cmbName.SelectionBoxItem;
            }
            catch (Exception)
            {
                MessageBox.Show("Valige meeskond!", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            var reg = await _vm.RegistrateTeam(Competition, pit);
            if (reg == null)
            {
                MessageBox.Show("Meeskond on registreeritud!", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Competitions newWindow = new Competitions(Competition);
            newWindow.Show();
            this.Close();
        }
    }
}
