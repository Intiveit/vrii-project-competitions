﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for Competitions.xaml
    /// </summary>
    public partial class Competitions : Window
    {
        private CompetitionVM _vm;
        private Competition _competition;
        

        public Competitions(Competition competition)
        {
            _competition = competition;
            InitializeComponent();
            this.Loaded += Competition_Loaded;

        }
        private void Competition_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new CompetitionVM();
            Task.Run(() => _vm.LoadData(_competition)).Wait();
            this.DataContext = _vm;

            if (StaticValues.Role == "participant" || StaticValues.Role == "admin")
            {
                btnRegister.Visibility = Visibility.Visible;
                btnDeleteRegistration.Visibility = Visibility.Visible;
            }
            if (StaticValues.Email == _competition.ApplicationUserId || StaticValues.Role == "admin")
            {
                btnDeleteCompetition.Visibility = Visibility.Visible;
                btnEdit.Visibility = Visibility.Visible;
                btnRegister.Visibility = Visibility.Visible;
                btnDeleteRegistration.Visibility = Visibility.Visible;

                if (DateTime.Now >= _competition.Time) btnAddResult.Visibility = Visibility.Visible;
            }
            if (DateTime.Now >= _competition.Time)
            {
                btnRegister.Visibility = Visibility.Hidden;
                btnDeleteRegistration.Visibility = Visibility.Hidden;
                lboxParticipants.ItemsSource = _vm.Participants;
                if (_vm.CompetitionType.HasTime) 
                lboxParticipants.Columns[3].Visibility = Visibility.Visible;
                if (_vm.CompetitionType.HasPoints)
                lboxParticipants.Columns[4].Visibility = Visibility.Visible;
                lboxParticipants.Columns[5].Visibility = Visibility.Visible;
            }

            lblName.Content = _competition.Name;
            lblTypeName.Content = _vm.CompetitionType.Name;
            lblPlace.Content = _vm.CompetitionPlace.Address;
            lblTime.Content = _competition.Time.ToString("dd.MM.yyyy");
            if (_vm.CompetitionType.IsIndividual)
            {
                lblType.Content = "individuaalne";
                lboxParticipants.Columns[2].Visibility = Visibility.Collapsed;
            }
            else
            {
                lblType.Content = "meeskondlik";
                lboxParticipants.Columns[0].Visibility = Visibility.Collapsed;
                lboxParticipants.Columns[1].Visibility = Visibility.Collapsed;
            }

            lblComment.Content = _competition.Comment;

            _competition.CompetitionPlace = _vm.CompetitionPlace;
            _competition.CompetitionType = _vm.CompetitionType;


        }

        
        private void btnRegister_Click(object sender, RoutedEventArgs e)
        {
            if (_vm.CompetitionType.IsIndividual)
            {
                RegisterToCompetition newWindow = new RegisterToCompetition(_competition);
                newWindow.Show();
                this.Close();
            }
            else
            {
                RegistrateTeam newWindow = new RegistrateTeam(_competition);
                newWindow.Show();
                this.Close();
            }
        }


        private async void btnDeleteRegistration_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Soovid kindlasti registreeringut kustutada?", "Registreeringu kustutamine?",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                Registration person = lboxParticipants.SelectedItem as Registration;

                var registration = await _vm.DeleteRegistration(_competition, person);
                if (registration == null)
                {
                    MessageBox.Show("Kustutamine ebaõnnestus");
                    return;
                }

                var newCompetitions = new Competitions(_competition);
                newCompetitions.Show();
                this.Close();
            }

        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow newWindow = new MainWindow();
            newWindow.Show();
            this.Close();
        }

        private async void btnDeleteCompetition_Click_1(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Soovid kindlasti seda võistlust kustutada?", "Võistluse kustutamine?",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                var competition = await _vm.DeleteCompetition(_competition);
                MainWindow newWindow = new MainWindow();
                newWindow.Show();
                this.Close();
            }

        }

        private void btnAddResult_Click(object sender, RoutedEventArgs e)
        {
            Results newWindow = new Results(_competition);
            newWindow.Show();
            this.Close();

        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            AddCompetition newWindow = new AddCompetition(_competition);
            newWindow.Show();
            this.Close();
        }
    }
}
