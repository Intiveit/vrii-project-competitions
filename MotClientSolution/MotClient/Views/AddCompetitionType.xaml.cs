﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for AddCompetitionType.xaml
    /// </summary>
    public partial class AddCompetitionType : Window
    {
        private AddCompetitionTypeVM _vm;
        public AddCompetitionType()
        {
            InitializeComponent();
            _vm = new AddCompetitionTypeVM();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            var newAddCompetition = new AddCompetition();
            newAddCompetition.Show();
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (txtTypeName.Text == "")
            {
                MessageBox.Show("Tüübi nimetus peab olema täidetud", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            _vm.CreateCompetitionType(txtTypeName.Text,cbxPoints.IsChecked.GetValueOrDefault(), cbxTime.IsChecked.GetValueOrDefault(), cbxIndividual.IsChecked.GetValueOrDefault()  );

            var newAddCompetition = new AddCompetition();
            newAddCompetition.Show();
            this.Close();
        }
    }
}
