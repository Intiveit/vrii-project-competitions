﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for MyCompetitions.xaml
    /// </summary>
    public partial class MyCompetitions : Window
    {
        private MyCompetitionsVM _vm;

        public MyCompetitions()
        {
            InitializeComponent();
            this.Loaded += MyCompetition_Loaded;
        }
        private void MyCompetition_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new MyCompetitionsVM();
            Task.Run(() => _vm.LoadData()).Wait();
            this.DataContext = _vm;

            lblName.Content = _vm.Person.FirstName + " " + _vm.Person.LastName;
            lblIdCode.Content = _vm.Person.PersonalIdCode;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow newWindow = new MainWindow();
            newWindow.Show();
            this.Close();
        }

        private async void txtRegistrations_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                Registration reg = ((FrameworkElement)sender).DataContext as Registration;
                Competition comp = await _vm.GetCompetitionForRegistration(reg);
                Competitions competitions = new Competitions(comp);
                competitions.Show();
                this.Close();
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            EditUser newWindow = new EditUser(_vm.Person);
            newWindow.Show();
            this.Close();
        }
    }
}
