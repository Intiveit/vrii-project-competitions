﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for AddNotUser.xaml
    /// </summary>
    public partial class AddNotUser : Window
    {
        private Team _team;
        private AddNotUserVM _vm;

        public AddNotUser(Team team)
        {
            _team = team;
            _vm = new AddNotUserVM();
            InitializeComponent();
        }

        private async void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (txtFirstName.Text == "" || txtLastName.Text == "")
            {
                MessageBox.Show("Kõik väljad peavad olema täidetud!", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            await _vm.AddTeamMember(_team, txtFirstName.Text, txtLastName.Text);
            Members newWindow = new Members(_team);
            newWindow.Show();
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Members newWindow = new Members(_team);
            newWindow.Show();
            this.Close();
        }
    }
}
