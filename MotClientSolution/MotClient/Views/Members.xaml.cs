﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for Members.xaml
    /// </summary>
    public partial class Members : Window
    {
        private MembersVM _vm;
        private Team _team;

        public Members(Team team)
        {
            _team = team;
            InitializeComponent();
            this.Loaded += Members_Loaded;
        }
        private void Members_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new MembersVM(_team);
            Task.Run(() => _vm.LoadData()).Wait();
            this.DataContext = _vm;


        }

        private void btnNUser_Click(object sender, RoutedEventArgs e)
        {
            AddNotUser newWindow = new AddNotUser(_team);
            newWindow.Show();
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            TeamInfo newWindow = new TeamInfo(_team);
            newWindow.Show();
            this.Close();
        }

        private void btnUser_Click(object sender, RoutedEventArgs e)
        {
            AddUser newWindow = new AddUser(_team);
            newWindow.Show();
            this.Close();
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Soovid kindlasti tiimi liiget kustutada?", "Liikme kustutamine?",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                Person person = lboxMembers.SelectedItem as Person;

                if (person == null)
                {
                    MessageBox.Show("Valige osaleja, kellele tulemust lisada");
                    return;
                }

                var pit = await _vm.DeleteMember(_team, person);
                if (pit == null)
                {
                    MessageBox.Show("Kustutamine ebaõnnestus");
                    return;
                }

                var newCompetitions = new Members(_team);
                newCompetitions.Show();
                this.Close();
            }
        }
    }
}
