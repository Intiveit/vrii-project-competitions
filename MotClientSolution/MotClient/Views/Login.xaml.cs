﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.ViewModel;

namespace MotClient.Views
{

    public partial class Login : Window
    {
        private LoginVM _vm;

        public Login()
        {
            _vm = new LoginVM();
            InitializeComponent();
        }

        private async void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            var token = await _vm.Login(txtUser.Text, txtPassword.Password);
            if (token == null)
            {
                MessageBox.Show("Sisselogimine ebaõnnestus.", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            MainWindow mw = new MainWindow();
            mw.Show();
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow newWindow = new MainWindow();
            newWindow.Show();
            this.Close();
        }
    }
}
