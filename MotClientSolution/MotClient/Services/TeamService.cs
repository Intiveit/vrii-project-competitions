﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;

namespace MotClient.Services
{
    public class TeamService:BaseService
    {
        public TeamService() : base(Config.TeamServiceUrl)
        {

        }

        public async Task<List<PersonInTeam>> GetAllTeams()
        {
            return await base.Get<List<PersonInTeam>>("");
        }

        public async Task<Team> GetById(int id)
        {
            return await base.Get<Team>("teams/" + id.ToString());
        }
        public async Task<List<PersonInTeam>> GetTeamsByPerson(int id)
        {
            return await base.Get<List<PersonInTeam>>("teams/person/" + id.ToString());
        }

        public async Task<Team> AddNewTeam(Team t)
        {
            return await base.Post("", t);
        }
        public async Task<PersonInTeam> AddNewPersonInTeam(int teamId, int personId)
        {
            return await base.Post("teams/" + teamId.ToString() + "/" + personId.ToString(), new PersonInTeam{});
        }

        public async Task<Team> DeleteById(int id)
        {
            return await base.Delete<Team>("teams/" + id.ToString());
        }

        public async Task<PersonInTeam> DeletePersonInTeam(int teamId, int personId)
        {
            return await base.Delete<PersonInTeam>("teams/" + teamId.ToString() + "/" + personId.ToString());
        }

        public async Task<Team> UpdateTeam(int id, Team t)
        {
            return await base.Put<Team>("teams/" + id.ToString(), t);
        }
    }
}
