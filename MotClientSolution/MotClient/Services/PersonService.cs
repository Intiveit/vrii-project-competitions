﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;

namespace MotClient.Services
{
    class PersonService : BaseService
    {



        public PersonService() : base(Config.PersonServiceUrl)
        {
        }

        public async Task<List<Person>> GetAllPeople()
        {
            return await base.Get<List<Person>>("");
        }

        public async Task<Person> GetById(int id)
        {
            return await base.Get<Person>("people/" + id.ToString());
        }
        public async Task<Person> GetByIdCode(string idCode)
        {
            return await base.Get<Person>("people/idcode/" + idCode);
        }
        public async Task<Person> GetPersonByEmail(string email)
        {
            return await base.Get<Person>("people/email/" + email);
        }

        public async Task<List<Person>> GetPeopleByTeam(int teamId)
        {
            return await base.Get<List<Person>>("people/team/" + teamId.ToString());
        }

        public async Task<Person> AddNewPerson(Person p)
        {
            return await base.Post("", p);
        }
        public async Task<Person> UpdatePerson(int id, Person p)
        {
            return await base.Put<Person>("people/" + id.ToString(), p);
        }

        public async Task<Person> DeleteById(int id)
        {
            return await base.Delete<Person>("people/" + id.ToString());
        }
    }
}
