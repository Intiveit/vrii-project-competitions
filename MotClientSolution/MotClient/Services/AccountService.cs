﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using Newtonsoft.Json;

namespace MotClient.Services
{
    class AccountService : BaseService
    {
        private HttpClient _client;
        public AccountService() : base(Config.AccountServiceUrl)
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(Config.AccountServiceUrl);
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", StaticValues.Token);
        }

        public async Task<List<User>> GetOrganizers()
        {
            var response = await _client.GetAsync("/api/account/organizers");
            return await response.Content.ReadAsAsync<List<User>>();
        }

        public async Task DeleteUser(string userId)
        {
            var response = await _client.DeleteAsync("/api/account/" + userId);
        }

        public async Task<User> Login(User u)
        {
            var response = await _client.PostAsJsonAsync("/api/account/gettoken", u);
            try
            {
                return await response.Content.ReadAsAsync<User>();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<User> Register(User u)
        {
            var response = await _client.PostAsJsonAsync("/api/account/register", u);
            try
            {
                return await response.Content.ReadAsAsync<User>();
            }
            catch (JsonSerializationException e)
            {
                return null;
            }
        }
    }
}
