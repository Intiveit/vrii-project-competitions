﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;

namespace MotClient.Services
{
    class ParticipationService : BaseService
    {
        public ParticipationService() : base(Config.ParticipationServiceUrl) { }

        public async Task<List<Participation>> GetAllParticipations()
        {
            return await base.Get<List<Participation>>("");
        }

        public async Task<Participation> GetById(int id)
        {
            return await base.Get<Participation>("participations/" + id.ToString());
        }
        public async Task<List<Participation>> GetAllParticipationsForCompetition(int competitionId)
        {
            return await base.Get<List<Participation>>("participations/competition/" + competitionId.ToString());
        }
        public async Task<List<Participation>> GetAllParticipationsForPerson(int personId)
        {
            return await base.Get<List<Participation>>("participations/person/" + personId.ToString());
        }

        public async Task<List<Participation>> GetAllParticipationsForTeam(int teamId)
        {
            return await base.Get<List<Participation>>("participations/team/" + teamId.ToString());
        }


        public async Task<Participation> AddNewParticipation(Participation p)
        {
            return await base.Post("", p);
        }

        public async Task<Participation> DeleteById(int id)
        {
            return await base.Delete<Participation>("participations/" + id.ToString());
        }

        public async Task<Participation> UpdateParticipation(int id, Participation p)
        {
            return await base.Put<Participation>("participations/" + id.ToString(), p);
        }

    }
}
