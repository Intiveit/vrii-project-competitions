﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    class AddCompetitionVM
    {
        private List<CompetitionPlace> _competitionPlaces;
        private List<CompetitionType> _competitionTypes;
        private CompetitionPlaceService _competitionPlaceService;
        private CompetitionTypeService _competitionTypeService;
        private CompetitionService _competitionService;


        public List<CompetitionPlace> CompetitionPlaces
        {
            get { return _competitionPlaces; }
            set
            {
                _competitionPlaces = value;
            }
        }

        public List<CompetitionType> CompetitionTypes
        {
            get { return _competitionTypes; }
            set
            {
                _competitionTypes = value;
            }
        }


        public AddCompetitionVM()
        {
            CompetitionTypes = new List<CompetitionType>();
            CompetitionPlaces = new List<CompetitionPlace>();
            _competitionPlaceService = new CompetitionPlaceService();
            _competitionTypeService = new CompetitionTypeService();
            _competitionService = new CompetitionService();
        }

        public async Task LoadData()
        {
            CompetitionTypes = await _competitionTypeService.GetAllCompetitionTypes();
            CompetitionPlaces = await _competitionPlaceService.GetAllCompetitionPlaces();

        }


        public async Task<Competition> AddCompetition(string txtName, CompetitionType type, CompetitionPlace place, DateTime toDateTime, string txtComment)
        {
            Competition competition = new Competition()
            {
                Name = txtName,
                CompetitionTypeId = type.CompetitionTypeId,
                CompetitionPlaceId = place.CompetitionPlaceId,
                Time = toDateTime,
                Comment = txtComment,
                ApplicationUserId = StaticValues.Email
            };

            return await _competitionService.AddNewCompetition(competition);
        }


        public async Task<Competition> UpdateCompetition(string txtName, CompetitionType type, CompetitionPlace place, DateTime toDateTime, string txtComment, Competition competition)
        {
            competition.Name = txtName;
            competition.CompetitionTypeId = type.CompetitionTypeId;
            competition.CompetitionPlaceId = place.CompetitionPlaceId;
            competition.Time = toDateTime;
            competition.Comment = txtComment;

            return await _competitionService.UpdateCompetition(competition.CompetitionId, competition);

        }
    }
}
