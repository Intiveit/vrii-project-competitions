﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    public class AddNotUserVM
    {
        private TeamService _teamService;
        private PersonService _personService;

        public AddNotUserVM()
        {
            _teamService = new TeamService();
            _personService = new PersonService();
        }

        public async Task<PersonInTeam> AddTeamMember(Team team, string firstName, string lastName)
        {
            var p = new Person()
            {
                FirstName = firstName,
                LastName = lastName
            };
            var newPerson = await _personService.AddNewPerson(p);

            return await _teamService.AddNewPersonInTeam(team.TeamId, newPerson.PersonId);
        }
    }
}
