﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    public class AdminViewVM
    {
        private PersonService _personService;
        private AccountService _accountService;
        private List<Person> _people;
        private List<User> _organizers;

        public List<Person> People
        {
            get { return _people; }
            set { _people = value; }
        }

        public List<User> Organizers
        {
            get { return _organizers; }
            set { _organizers = value; }
        }

        public AdminViewVM()
        {
            _personService = new PersonService();
            _accountService = new AccountService();
        }

        public async Task LoadData()
        {
            var p = await _personService.GetAllPeople();
            People = p;
            var o = await _accountService.GetOrganizers();
            Organizers = o;
        }

        public async Task<Person> DeleteParticipant(Person person)
        {
            await _accountService.DeleteUser(person.ApplicationUserId);
            return await _personService.DeleteById(person.PersonId);
        }

        public async Task DeleteOrganizer(User user)
        {
            await _accountService.DeleteUser(user.username);
        }
    }
}
