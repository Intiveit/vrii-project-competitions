﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    class ResultsVM
    {

        private ParticipationService _participationService;
        private List<Participation> _participatedPeople;

        public List<Participation> Participants
        {
            get { return _participatedPeople; }
            set
            {
                _participatedPeople = value;
            }
        }


        public ResultsVM()
        {
            Participants = new List<Participation>();
            _participationService = new ParticipationService();
        }
        public async Task LoadData(Competition competition)
        {
            var participation = await _participationService.GetAllParticipationsForCompetition(competition.CompetitionId);
            Participants = participation;
        }

        /// <summary>
        /// Updates person or teams results
        /// </summary>
        public async Task<Participation> AddResult(Participation person, Competition competition, string points, string hours, string minutes, string seconds, bool isDisqualified)
        {
            var allParticipations = await _participationService.GetAllParticipationsForCompetition(competition.CompetitionId);
            Participation part;
            if (person.PersonId != null)
            {
                part = allParticipations.FirstOrDefault(x => x.PersonId == person.PersonId);
            }
            else
            {
                part = allParticipations.FirstOrDefault(x => x.TeamId == person.TeamId);
            }

            int p, h, m, s;
            if (!Int32.TryParse(points, out p))
            {
                p = 0;
            }
            if (!Int32.TryParse(hours, out h))
            {
                h = 0;
            }
            if (!Int32.TryParse(minutes, out m) || m >= 60)
            {
                m = 0;
            }
            if (!Int32.TryParse(seconds, out s) || s >= 60)
            {
                s = 0;
            }
            if (part == null)
            {
                Participation newParticipation = new Participation()
                {
                    PersonId = person.PersonId,
                    CompetitionId = competition.CompetitionId,
                    Time = new TimeSpan(h, m, s),
                    Points = p,
                    IsDisqualified = isDisqualified
                };

                return await _participationService.AddNewParticipation(newParticipation);
            }
            else
            {
                part.Time = new TimeSpan(h, m, s);
                part.Points = p;
                part.IsDisqualified = isDisqualified;

                return await _participationService.UpdateParticipation(part.ParticipationId, part);
            }


        }

        public async  Task<Participation> DeleteParticipation(Competition competition, Participation person)
        {
            if (StaticValues.Role == "organizer" || StaticValues.Role == "admin")
            {
                var participations = await _participationService.GetAllParticipationsForCompetition(competition.CompetitionId);
                var participation = participations.FirstOrDefault(x => x.PersonId == person.PersonId);
                if (participation != null)
                {
                    var deletedRegistration = await _participationService.DeleteById(participation.ParticipationId);
                    return deletedRegistration;
                }
            }
            return null;

        }
    }
}
