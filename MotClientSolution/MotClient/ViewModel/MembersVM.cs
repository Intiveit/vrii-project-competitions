﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    public class MembersVM
    {
        private PersonService _personService;
        private TeamService _teamService;
        private List<Person> _people;

        public Team Team;

        public List<Person> People
        {
            get { return _people; }
            set { _people = value; }
        }


        public MembersVM(Team team)
        {
            Team = team;
            _personService = new PersonService();
            _teamService = new TeamService();
        }
        public async Task LoadData()
        {
            var p = await _personService.GetPeopleByTeam(Team.TeamId);
            People = p;
        }

        public async Task<PersonInTeam> DeleteMember(Team team, Person person)
        {
            var pit = await _teamService.DeletePersonInTeam(team.TeamId, person.PersonId);
            return pit;

        }
    }
}
