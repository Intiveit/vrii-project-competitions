﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    
    public class EditUserVM
    {
        private PersonService _personService;

        public EditUserVM()
        {
            _personService= new PersonService();
        }
        public async Task<Person> UpdatePerson(Person person, string firstName, string lastName, string personalIDCode)
        {
            Person p = new Person()
            {
                ApplicationUserId = person.ApplicationUserId,
                FirstName = firstName,
                LastName = lastName,
                PersonalIdCode = personalIDCode
            };
            return await _personService.UpdatePerson(person.PersonId, p);
        }
    }
}
