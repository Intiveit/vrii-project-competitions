﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotClient.Model
{
    class Response
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
