﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotClient.Model
{
    public class Competition
    {
        public int CompetitionId { get; set; }

        public string Name { get; set; }

        public int CompetitionTypeId { get; set; }
        public CompetitionType CompetitionType { get; set; }

        public int CompetitionPlaceId { get; set; }
        public CompetitionPlace CompetitionPlace { get; set; }

        public DateTime Time { get; set; }

        public string Comment { get; set; }

        public string ApplicationUserId { get; set; }

        public List<Registration> Registrations { get; set; } = new List<Registration>();
        public List<Participation> Participations { get; set; } = new List<Participation>();
    }
}
