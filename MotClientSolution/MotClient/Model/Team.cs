﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotClient.Model
{
    public class Team
    {
        public int TeamId { get; set; }

        public string Name { get; set; }

        public List<PersonInTeam> PeopleInTeam { get; set; } = new List<PersonInTeam>();
    }
}
