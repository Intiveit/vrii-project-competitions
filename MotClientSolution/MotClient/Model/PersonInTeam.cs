﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotClient.Model
{
    public class PersonInTeam
    {
        public int PersonInTeamId { get; set; }

        public int PersonId { get; set; }
        public Person Person { get; set; }

        public int TeamId { get; set; }
        public Team Team { get; set; }
        public string TeamName { get; set; }
    }
}
