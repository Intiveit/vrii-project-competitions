﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;
using MotClient.Views;

namespace MotClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowVM _vm;
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;

        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new MainWindowVM();
            _vm.LoadData();
            this.DataContext = _vm;
            if (StaticValues.Token != null)
            {
                btnLogin.Visibility = Visibility.Hidden;
                btnRegistrate.Visibility = Visibility.Hidden;
                btnLogout.Visibility = Visibility.Visible;

                if (StaticValues.Role == "participant")
                {
                    btnMyData.Visibility = Visibility.Visible;
                    btnMyTeams.Visibility = Visibility.Visible;
                }
            }

            if (StaticValues.Role == "organizer" || StaticValues.Role == "admin")
            {
                btnAddCompetition.Visibility = Visibility.Visible;
            }

            if (StaticValues.Role == "admin")
            {
                btnAdminView.Visibility = Visibility.Visible;
            }



        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            Login newLogin = new Login();
            newLogin.Show();
            this.Close();
        }

        private void btnRegisteeri_Click(object sender, RoutedEventArgs e)
        {
            Registreeri newWindow = new Registreeri();
            newWindow.Show();
            this.Close();
        }

        private void btnOtsi_Click(object sender, RoutedEventArgs e)
        {
            _vm.Search(txtSearch.Text, Convert.ToDateTime(dpDate.SelectedDate));
            lboxSearch.ItemsSource = _vm.SearchResults;
            lboxSearch.Items.Refresh();
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            StaticValues.Token = null;
            StaticValues.Role = null;
            StaticValues.Email = null;
            
            btnLogin.Visibility = Visibility.Visible;
            btnRegistrate.Visibility = Visibility.Visible;
            btnLogout.Visibility = Visibility.Hidden;
            btnAddCompetition.Visibility = Visibility.Hidden;
            btnMyData.Visibility = Visibility.Hidden;
            btnMyTeams.Visibility = Visibility.Hidden;
            btnAdminView.Visibility = Visibility.Hidden;
        }

        private void btnAddCompetition_Click(object sender, RoutedEventArgs e)
        {
            var newWindow = new AddCompetition();
            newWindow.Show();
            this.Close();
        }

        private void txtCompetition_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                Competition comp = ((FrameworkElement) sender).DataContext as Competition;
                Competitions competitions = new Competitions(comp);
                competitions.Show();
                this.Close();

            }
        }

        private void txtOtsing_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                Competition comp = ((FrameworkElement)sender).DataContext as Competition;
                Competitions competitions = new Competitions(comp);
                competitions.Show();
                this.Close();
            }
        }

        private void btnMyData_Click(object sender, RoutedEventArgs e)
        {
            MyCompetitions newWindow = new MyCompetitions();
            newWindow.Show();
            this.Close();
        }

        private void btnMyTeams_Click(object sender, RoutedEventArgs e)
        {
            TeamList newWindow = new TeamList();
            newWindow.Show();
            this.Close();
        }

        private void btnAdminView_Click(object sender, RoutedEventArgs e)
        {
            AdminView newWindow = new AdminView();
            newWindow.Show();
            this.Close();
        }
    }
}
