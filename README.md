# Veebirakendused II projekt - 

Antud projekt koosneb kahest osast veebiteenusest - MotSolution ja klientrakendusest MotClientSolution

Antud projekt on v�istluste infos�steem, kust saab infot v�istluste kohta ja neile registeerida.

## Infos�steemi funktsioonid: 
 
* Saab registreerida kasutajaid korraldaja v�i osaleja rollis

** Korraldaja **

- Korraldaja saab lisada v�istluse toimumiskohti 
- Korraldaja saab lisada v�istlusi
- Korraldaja saab muuta enda lisatud v�istluse andmeid
- Korraldaja saab kustutada enda lisatud v�istlusi
- Korraldaja n�eb v�istlejate nimekirja ja nende andmeid
- Korraldaja saab muuta enda kui kasutaja andmeid
- Korraldaja saab lisada uusi spordialasid 
- Korraldaja saab lisada v�istluse tulemusi (peale toimumist)

** Osaleja**

* Osaleja n�eb sisestatud v�istlusi
* Osaleja n�eb, kui palju on mingile v�istlusele osalejaid registreeritud
* Osaleja saab otsida v�istlusi toimumiskoha ja -aja j�rgi
* Osaleja saab registreeruda v�istlustele
* Osaleja n�eb v�istlusi, kuhu ta on registreerunud
* Osaleja saab oma registreeringu t�histada
* Osaleja saab muuta enda kui kasutaja andmeid
* Osaleja saab luua meeskondi
* Osaleja saab registreerida v�istlusele meeskonda
* Osaleja saab registreerida v�istlusele teise isiku 
* Osaleja n�eb statistikat enda m��dunud v�istluste tulemuste kohta

** Meeskond**

- Meeskonna liige saab muuta meeskonna liikmeid
- Meeskonna liige saab muuta meeskonna andmeid
- Meeskonna liige saab vaadata meeskonna tulemusi

** Registeerimata kasutaja **

* Registeerimata kasutaja n�eb sisestatud v�istlusi
* Registeerimata kasutaja n�eb, kui palju on mingile v�istlusele osalejaid registreeritud
* Registeerimata kasutaja saab otsida v�istlusi toimumiskoha ja -aja j�rgi

** Administraator **

- Administraator saab teha k�ike eelnevat
- Administraator saab kustutada/muuta/lisada kasutajaid


**NB!** Admin kasutaja andmed: **Kasutajanimi:** admin@admin.com   **Parool:** Parool1
